## Update

```bash
cd php/7.3
docker build --no-cache -t registry.gitlab.com/frankverhoeven/docker/php:7.3 .
docker push registry.gitlab.com/frankverhoeven/docker/php:7.3
```


## Login to Repository

```bash
docker login registry.gitlab.com
```
